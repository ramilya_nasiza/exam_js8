import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://exam8-f3841.firebaseio.com/'
});

export default instance;