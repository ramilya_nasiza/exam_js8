import React, {Component, Fragment} from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import MainPage from "./containers/MainPage/MainPage";

import Header from "./components/Header/Header";

import './App.css';
import AddQuote from "./containers/AddQuote/AddQuote";
import CategoryPage from "./containers/CategoryPage/CategoryPage";
import Edit from "./containers/Edit/Edit";


class App extends Component {

  state = {
      urls: [
        {path: '/', title: 'quotes', exact: true},
        {path: '/add-quote', title: 'Submit new quote', exact: true},
      ]
  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Fragment>
            <Header links={this.state.urls}/>
            <Switch>
              <Route path='/' exact component={MainPage}/>
              <Route path='/add-quote' exact component={AddQuote}/>
              <Route path='/quotes/:category' component={CategoryPage}/>
              <Route path='/quotes/all' component={MainPage}/>
              <Route path="/quotes/:id/edit" component={Edit}/>
            </Switch>
          </Fragment>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
