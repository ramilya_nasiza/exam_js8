import React, {Component} from 'react';
 import Quote from "../../components/Quote/Quote";
import axios from '../../axios-quote';

import './MainPage.css';
import CategoryMenu from "../../components/CategoryMenu/CategoryMenu";


class MainPage extends Component {

  state = {
    quotes: null
  };

  componentDidMount() {
    axios.get('/quotes.json').then((response) => {
      const quotes = Object.values(response.data);
      const keys = Object.keys(response.data);

      for (let key in quotes) {
        quotes[key].id = keys[key];
      }

      this.setState({quotes});
    })
  }


  render() {

    const categories =  [
      {path: '/quotes/star-wars', title: 'Star Wars'},
      {title: 'Famous people', path: '/quotes/famous-people'},
      {title: 'Saying', path: '/quotes/saying'},
      {title: 'Humor', path: '/quotes/humor'},
      {title: 'All', path: '/'}
    ];

    let quotes = () => {
      if (this.state.quotes) {
        return this.state.quotes.map((quote) => (
            <Quote
                key={quote.id}
                text={quote.text}
                author={quote.author}
                href={'/quotes/' + this.props.match.params.id + '/edit'}
            />
        ))
      } else {
        return (<div>No quotes here...</div>)
      }
    };

    return (
        <div className='MainPage'>
            <CategoryMenu
                allcategories={categories}
            />
            <div className='quotesBlock'>
              {quotes()}
            </div>
        </div>
    );
  }
}

export default MainPage;