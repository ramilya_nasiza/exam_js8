import React, {Component} from 'react';
import axios from '../../axios-quote';
import Form from "../../components/Form/Form";

class Edit extends Component {
  state = {
    category: 'star-wars',
    author: '',
    text: ''
  };

  componentDidMount() {
    const id = this.props.match.params.id;

    axios.get('/quotes/' + id + '.json').then(quote => {
      this.setState({
        title: quote.data.title,
        description: quote.data.description,
        date: quote.data.date,
        id
      });
    })
  }

  changeValueHandler = (event) => {
    const thisName = event.target.name;
    this.setState({[thisName]: event.target.value});
  };

  saveChangesHandler = (event) => {
    event.preventDefault();
    axios.put('/quotes/' + this.state.id + '.json', this.state).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
        <div className="Edit">
            <Form
                changeValue={(event) => this.changeValueHandler(event)}
                createQuote={(event) => this.saveChangesHandler(event)}
                author={this.state.author}
                text={this.state.text}
                category={this.state.category}
            />
        </div>
    );
  }
}

export default Edit;