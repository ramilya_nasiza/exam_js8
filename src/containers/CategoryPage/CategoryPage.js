import React, {Component} from 'react';
import axios from '../../axios-quote';
import Quote from "../../components/Quote/Quote";

import './CategoryPage.css';
import CategoryMenu from "../../components/CategoryMenu/CategoryMenu";

class CategoryPage extends Component {

  state = {
    quotes: null
  };


  componentDidMount() {

    const categoryLink = '/quotes.json?orderBy="category"&equalTo=';

    axios.get(categoryLink + '"' + this.props.match.params.category + '"').then(response => {

      const quotes = Object.values(response.data);
      const keys = Object.keys(response.data);

      for (let key in quotes) {
        quotes[key].id = keys[key];
      }

      this.setState({quotes});
    });
  }


  deleteQuote = () => {
    axios.delete('/quotes.json?orderBy="category"&equalTo="' + this.props.match.params.category + '"').then(() => {
      this.props.history.replace('/quotes');
    })
  };


  render() {

    const categories =  [
      {path: '/quotes/star-wars', title: 'Star Wars'},
      {title: 'Famous people', path: '/quotes/famous-people'},
      {title: 'Saying', path: '/quotes/saying'},
      {title: 'Humor', path: '/quotes/humor'},
      {title: 'All', path: '/'}
    ];

    let quotes = () => {
      if (this.state.quotes) {
        return this.state.quotes.map((quote) => (
            <Quote
                key={quote.id}
                text={quote.text}
                author={quote.author}
                delete={() => this.deleteQuote()}
                href={'/quotes/' + this.props.match.params.id + '/edit'}
            />
        ))
      } else {
        return (<div>No quotes in this category</div>)
      }
    };

    return (
        <div className='CategoryPage'>
          <CategoryMenu allcategories={categories}/>
          <div className='ExactCategories'>
            {quotes()}
          </div>
        </div>

    );
  }
}

export default CategoryPage;