import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import axios from '../../axios-quote';


import './AddQuote.css';
import Container from "../../components/Container/Container";


class AddQuote extends Component {

  state = {
    category: 'star-wars',
    author: '',
    text: ''
  };

  changeValueHandler = (event) => {
    const thisName = event.target.name;
    this.setState({[thisName]: event.target.value});
  };

  createQuote = (event) => {
    event.preventDefault();
    if (this.state.author && this.state.text && this.state.category) {

      const quote = {...this.state};

      axios.post('/quotes.json', quote).finally(() => {
        this.setState({author: '', text: ''});
      });
    } else {
      alert('Fields are empty!');
    }
  };

  render() {
    return (
        <div className='AddQuote'>
          <Container>
            <h3 className='AddQuoteTitle'>Submit new quote</h3>
            <Form
                changeValue={(event) => this.changeValueHandler(event)}
                createQuote={(event) => this.createQuote(event)}
                author={this.state.author}
                text={this.state.text}
                category={this.state.category}
            />
          </Container>
        </div>
    );
  }
}

export default AddQuote;