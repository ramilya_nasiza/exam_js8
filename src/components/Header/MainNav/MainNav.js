import React from 'react';
import {NavLink} from 'react-router-dom';
import './MainNav.css';

const MainNav = props => {
  return (
      <nav className="MainNav">
        <ul>
          {props.links.map((link, id) => (
              <li key={id}>
                <NavLink to={link.path} exact={link.exact}>{link.title}</NavLink>
              </li>
          ))}
        </ul>
      </nav>
  );
};

export default MainNav;