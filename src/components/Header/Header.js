import React from 'react';
import Container from "../Container/Container";
import {NavLink} from 'react-router-dom';
import MainNav from "./MainNav/MainNav";

import './Header.css';

const Header = props => {
  return (
      <header className="Header">
        <Container>
          <NavLink to="/">Quotes Central</NavLink>
          <MainNav links={props.links}/>
        </Container>
      </header>
  );
};

export default Header;