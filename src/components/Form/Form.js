import React from 'react';

import './Form.css';

const Form = props => {
  return (
      <div className="FormBlock">
        <form className='Form'>
          <div className='FormItem'>
            <label htmlFor="category">Category</label>
            <select value={props.category} name='category' onChange={props.changeValue}>
              <option value="star-wars">Star Wars</option>
              <option value="famous-people">Famous people</option>
              <option value="saying">Saying</option>
              <option value="humor">Humor</option>
              <option value="motivation">Motivation</option>
            </select>
          </div>
          <div className='FormItem'>
            <label htmlFor="author">Author</label>
            <input value={props.author} type="text" name='author' onChange={props.changeValue}/>
          </div>
          <div className='FormItem'>
            <label htmlFor="text">Quote text</label>
            <textarea value={props.text} name='text' onChange={props.changeValue}/>
          </div>
          <button onClick={props.createQuote} type="submit">Submit</button>
        </form>
      </div>
      );
};

export default Form;