import React from 'react';

import './CategoryMenu.css';
import {Link} from "react-router-dom";

const CategoryMenu = props => {
  return (
      <div className='Categories'>
        <ul className='CategoriesList'>
          {props.allcategories.map((category) =>(
              <li key={category.path}>
                <Link
                    to={category.path}>
                  {category.title}
                </Link>
              </li>
          ))}
        </ul>
      </div>
  );
};

export default CategoryMenu;