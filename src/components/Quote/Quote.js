import React from 'react';

import './Quote.css';
import DeleteButton from "./DeleteButton/DeleteButton";
import {Link} from "react-router-dom";

const Quote = props => {
  return (
      <div className='Quote'>
        <p className='QuoteText'>"{props.text}"</p>
        <p className='QuoteAuthor'> - {props.author}</p>
        <DeleteButton delete={props.delete}/>
        <Link to={props.href}>Edit</Link>
      </div>
  );
};

export default Quote;