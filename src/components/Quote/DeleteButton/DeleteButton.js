import React from 'react';

import './DeleteButton.css';
const DeleteButton = props => {
  return (
      <button className='Delete' onClick={props.delete}>X</button>
  );
};

export default DeleteButton;